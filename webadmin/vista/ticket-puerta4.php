<?php

header('Access-Control-Allow-Origin: *');

require '../plugins-escpos/autoload.php';

use Mike42\Escpos\Printer;
use Mike42\Escpos\EscposImage;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;

class item
{
    private $name;
    private $price;
    private $dollarSign;

    public function __construct($name = '', $price = '', $dollarSign = false)
    {
        $this -> name = $name;
        $this -> price = $price;
        $this -> dollarSign = $dollarSign;
    }

    public function __toString()
    {
        $rightCols = 10;
        $leftCols = 38;
        if ($this -> dollarSign) {
            $leftCols = $leftCols / 2 - $rightCols / 2;
        }
        $left = str_pad($this -> name, $leftCols) ;

        $sign = ($this -> dollarSign ? 'S/. ' : '');
        $right = str_pad($sign . $this -> price, $rightCols, ' ', STR_PAD_LEFT);
        return "$left$right\n";
    }
}

$items = array(
    new item($_POST["tipo_personal"],$_POST["precio"])
);
$total = new item('TOTAL A PAGAR', $_POST["precio"], true);

$nombre_impresora = "EPSON TM-T20II Receipt"; 
  
$connector = new WindowsPrintConnector($nombre_impresora);
$printer = new Printer($connector);

$printer -> selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
$printer -> setEmphasis(true);
$printer->text("ECOMPHISA" . "\n");
$printer -> setEmphasis(false);
$printer->text("R.U.C. N° 20271407263" . "\n");
$printer->text("Prolong. Mariscal Castilla S/N Santa Rosa - Chiclayo - Lambayeque" . "\n");
$printer->text("---------------------------------------------------------" . "\n");
$printer -> setEmphasis(true);
$printer->text("RECIBO N° " .$_POST["id_ingreso"]."\n");
$printer -> setEmphasis(false);
$printer->text("---------------------------------------------------------" . "\n");
$printer->setJustification(Printer::JUSTIFY_LEFT);
$printer->text("Cliente   :  " .$_POST["nombre"]. "\n");
$printer->text("DNI/RUC   :  " .$_POST["dni"]. "\n");
$printer->text("Tipo      :  " .$_POST["tipo_personal"]. "\n");
$printer->text("Estado    :  PAGO" ."\n");
$printer->text("Cond. Pago:  CONTADO" . "\n");
$printer->text("Fecha     :  " .date("d-m-Y H:i"). "\n");
$printer->text("---------------------------------------------------------" . "\n");
$printer -> setJustification(Printer::JUSTIFY_LEFT);
$printer -> setEmphasis(true);
$printer -> text(new item('NOMBRE', 'PRECIO'));
$printer -> setEmphasis(false);
foreach ($items as $item) {
    $printer -> text($item);
}
$printer->text("---------------------------------------------------------" . "\n");
$printer -> selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
$printer -> text($total);
$printer -> feed(2);
$printer -> setJustification(Printer::JUSTIFY_CENTER);
$printer -> setEmphasis(true);
$printer->text("* GRACIAS POR SU VISITA *");
$printer -> setEmphasis(false);

$printer->cut();
 
$printer->close();
?>